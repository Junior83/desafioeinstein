package njdevelopment;

public class Atributos {
	
	public static final String[] COR = {"", "Amarela", "Azul", "Laranja", "Verde", "Vermelha"};
	public static final String[] NACIONALIDADE = {"", "Alemão", "Dinamarquês", "Inglês", "Norueguês", "Suéco"};
	public static final String[] BEBIDA = {"", "Água", "Café", "Cerveja", "Chá", "Leite"};
	public static final String[] CIGARRO = {"", "Blends", "Bluemaster", "Dunhill", "Pall Mall", "Prince"};
	public static final String[] ANIMAL = {"", "Cães", "Cavalos", "Gatos", "Pássaros", "Peixes"};

}
