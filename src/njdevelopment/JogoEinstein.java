package njdevelopment;

import java.awt.EventQueue;

import javax.swing.JFrame;
import java.awt.BorderLayout;
import javax.swing.JLabel;
import javax.swing.JComboBox;
import javax.swing.JFormattedTextField;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.ImageIcon;
import java.awt.Color;
import javax.swing.JTextArea;
import javax.swing.JCheckBox;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.Font;
import javax.swing.UIManager;
import javax.swing.JButton;

public class JogoEinstein {

	private JFrame frame;
	private JComboBox cbCorCasa1, cbCorCasa2, cbCorCasa3, cbCorCasa4, cbCorCasa5;
	private JComboBox cbNacionalidade1, cbNacionalidade2, cbNacionalidade3, cbNacionalidade4, cbNacionalidade5;
	private JComboBox cbBebida1, cbBebida2, cbBebida3, cbBebida4, cbBebida5;
	private JComboBox cbCigarro1, cbCigarro2, cbCigarro3, cbCigarro4, cbCigarro5;
	private JComboBox cbAnimal1, cbAnimal2, cbAnimal3, cbAnimal4, cbAnimal5;
	private JLabel lblDicas;
	private JCheckBox ckbxDica1, ckbxDica2, ckbxDica3, ckbxDica4 ,ckbxDica5, ckbxDica6, ckbxDica7, ckbxDica8;
	private JCheckBox ckbxDica9, ckbxDica10, ckbxDica11, ckbxDica12, ckbxDica13, ckbxDica14, ckbxDica15;
	private JLabel lblParabéns;
	private String corCasa1, corCasa2, corCasa3, corCasa4, corCasa5;
	
	public void confirmar(){
		
		if(cbCorCasa1.getSelectedItem().equals("Amarela"         ) && cbNacionalidade1.getSelectedItem().equals("Norueguês"  ) && cbBebida1.getSelectedItem().equals("Água"   ) && cbCigarro1.getSelectedItem().equals("Dunhill"   ) && cbAnimal1.getSelectedItem().equals("Gatos")
				&& cbCorCasa2.getSelectedItem().equals("Azul"    ) && cbNacionalidade2.getSelectedItem().equals("Dinamarquês") && cbBebida2.getSelectedItem().equals("Chá"    ) && cbCigarro2.getSelectedItem().equals("Blends"    ) && cbAnimal2.getSelectedItem().equals("Cavalos")
				&& cbCorCasa3.getSelectedItem().equals("Vermelha") && cbNacionalidade3.getSelectedItem().equals("Inglês"     ) && cbBebida3.getSelectedItem().equals("Leite"  ) && cbCigarro3.getSelectedItem().equals("Pall Mall" ) && cbAnimal3.getSelectedItem().equals("Pássaros")
				&& cbCorCasa4.getSelectedItem().equals("Verde"   ) && cbNacionalidade4.getSelectedItem().equals("Alemão"     ) && cbBebida4.getSelectedItem().equals("Café"   ) && cbCigarro4.getSelectedItem().equals("Prince"    ) && cbAnimal4.getSelectedItem().equals("Peixes")
				&& cbCorCasa5.getSelectedItem().equals("Laranja" ) && cbNacionalidade5.getSelectedItem().equals("Suéco"      ) && cbBebida5.getSelectedItem().equals("Cerveja") && cbCigarro5.getSelectedItem().equals("Bluemaster") && cbAnimal5.getSelectedItem().equals("Cães")
				){
			lblParabéns.setVisible(true);
		}else{
			lblParabéns.setVisible(false);
		}
	}
	
	public void zerar(){
		
		lblParabéns.setVisible(false);
		
		cbCorCasa1.setSelectedItem(""); cbCorCasa2.setSelectedItem(""); cbCorCasa3.setSelectedItem(""); cbCorCasa4.setSelectedItem(""); cbCorCasa5.setSelectedItem(""); 
		cbNacionalidade1.setSelectedItem(""); cbNacionalidade2.setSelectedItem(""); cbNacionalidade3.setSelectedItem(""); cbNacionalidade4.setSelectedItem(""); cbNacionalidade5.setSelectedItem(""); 
		cbBebida1.setSelectedItem(""); cbBebida2.setSelectedItem(""); cbBebida3.setSelectedItem(""); cbBebida4.setSelectedItem(""); cbBebida5.setSelectedItem(""); 
		cbCigarro1.setSelectedItem(""); cbCigarro2.setSelectedItem(""); cbCigarro3.setSelectedItem(""); cbCigarro4.setSelectedItem(""); cbCigarro5.setSelectedItem(""); 
		cbAnimal1.setSelectedItem(""); cbAnimal2.setSelectedItem(""); cbAnimal3.setSelectedItem(""); cbAnimal4.setSelectedItem(""); cbAnimal5.setSelectedItem("");
		
		ckbxDica1.setSelected(false); ckbxDica1.setText("O Norueguês vive na primeira casa."); ckbxDica2.setSelected(false); ckbxDica2.setText("O Inglês vive na casa Vermelha.");
		ckbxDica3.setSelected(false); ckbxDica3.setText("O Suéco têm Cães como animais de estimação."); ckbxDica4.setSelected(false); ckbxDica4.setText("O Dinamarquês bebe Chá.");
		ckbxDica5.setSelected(false); ckbxDica5.setText("A casa Verde fica do lado esquerdo da Laranja."); ckbxDica6.setSelected(false); ckbxDica6.setText("O homem que vive na casa Verde bebe Café");
		ckbxDica7.setSelected(false); ckbxDica7.setText("O homem que fuma Pall Mall cria Pássaros."); ckbxDica8.setSelected(false); ckbxDica8.setText("O homem que vive na casa Amarela fuma Dunhill.");
		ckbxDica9.setSelected(false); ckbxDica9.setText("O homem que vive na casa do meio bebe Leite."); ckbxDica10.setSelected(false); ckbxDica10.setText("O homem que fuma Blends vive ao lado do que tem Gatos.");
		ckbxDica11.setSelected(false); ckbxDica11.setText("O homem que cria Cavalos é vizinho do que fuma Dunhill."); ckbxDica12.setSelected(false); ckbxDica12.setText("O homem que fuma Bluemaster bebe Cerveja.");
		ckbxDica13.setSelected(false); ckbxDica13.setText("O Alemão fuma Prince."); ckbxDica14.setSelected(false); ckbxDica14.setText("O Norueguês vive ao lado da casa Azul.");
		ckbxDica15.setSelected(false); ckbxDica15.setText("O homem que fuma Blends é vizinho do que bebe Água.");
	}

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					JogoEinstein window = new JogoEinstein();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public JogoEinstein() {
		initialize();
		lblParabéns.setVisible(false);
		
		for(String cor : Atributos.COR){
			cbCorCasa1.addItem(cor);
			cbCorCasa2.addItem(cor);
			cbCorCasa3.addItem(cor);
			cbCorCasa4.addItem(cor);
			cbCorCasa5.addItem(cor);
		}
		
		for(String nacionalidade : Atributos.NACIONALIDADE){
			cbNacionalidade1.addItem(nacionalidade);
			cbNacionalidade2.addItem(nacionalidade);
			cbNacionalidade3.addItem(nacionalidade);
			cbNacionalidade4.addItem(nacionalidade);
			cbNacionalidade5.addItem(nacionalidade);
		}
		
		for(String bebida : Atributos.BEBIDA){
			cbBebida1.addItem(bebida);
			cbBebida2.addItem(bebida);
			cbBebida3.addItem(bebida);
			cbBebida4.addItem(bebida);
			cbBebida5.addItem(bebida);
		}
		
		for(String cigarro : Atributos.CIGARRO){
			cbCigarro1.addItem(cigarro);
			cbCigarro2.addItem(cigarro);
			cbCigarro3.addItem(cigarro);
			cbCigarro4.addItem(cigarro);
			cbCigarro5.addItem(cigarro);
		}
		
		for(String animal : Atributos.ANIMAL){
			cbAnimal1.addItem(animal);
			cbAnimal2.addItem(animal);
			cbAnimal3.addItem(animal);
			cbAnimal4.addItem(animal);
			cbAnimal5.addItem(animal);
		}
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setResizable(false);
		frame.getContentPane().setBackground(Color.WHITE);
		frame.setBounds(100, 100, 899, 492);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setLocationRelativeTo(null);
		frame.getContentPane().setLayout(null);
		frame.setTitle("Desafio Einstein");
		
		JLabel lblCor = new JLabel("CASA");
		lblCor.setFont(new Font("Apple Braille", Font.BOLD | Font.ITALIC, 13));
		lblCor.setBounds(6, 49, 115, 24);
		frame.getContentPane().add(lblCor);
		
		JLabel lblNacionalidade = new JLabel("NACIONALIDADE");
		lblNacionalidade.setFont(new Font("Apple Braille", Font.BOLD | Font.ITALIC, 13));
		lblNacionalidade.setBounds(6, 85, 115, 24);
		frame.getContentPane().add(lblNacionalidade);
		
		JLabel lblBebida = new JLabel("BEBIDA");
		lblBebida.setFont(new Font("Apple Braille", Font.BOLD | Font.ITALIC, 13));
		lblBebida.setBounds(6, 121, 115, 24);
		frame.getContentPane().add(lblBebida);
		
		JLabel lblCigarro = new JLabel("CIGARRO");
		lblCigarro.setFont(new Font("Apple Braille", Font.BOLD | Font.ITALIC, 13));
		lblCigarro.setBounds(6, 157, 115, 24);
		frame.getContentPane().add(lblCigarro);
		
		JLabel lblAnimal = new JLabel("ANIMAL");
		lblAnimal.setFont(new Font("Apple Braille", Font.BOLD | Font.ITALIC, 13));
		lblAnimal.setBounds(6, 193, 115, 24);
		frame.getContentPane().add(lblAnimal);
		
		cbNacionalidade1 = new JComboBox();
		cbNacionalidade1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				//===========================
				// Dica um
				
				if(cbNacionalidade1.getSelectedItem().equals("Norueguês")){
					ckbxDica1.setSelected(true);
					ckbxDica1.setText("<html><strike><font color=gray>O Norueguês vive na primeira casa.</font></strike></html>");
					
				}else{
					ckbxDica1.setSelected(false);
					ckbxDica1.setText("O Norueguês vive na primeira casa.");
				}
				//===========================
			}
		});
		cbNacionalidade1.setBounds(133, 73, 147, 50);
		frame.getContentPane().add(cbNacionalidade1);
		
		cbCorCasa1 = new JComboBox();
		cbCorCasa1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				//======================================
				// COR DA CASA 1
				
				corCasa1 = cbCorCasa1.getSelectedItem().toString();
				if(corCasa1.equals("Amarela")){
					cbCorCasa1.setForeground(new Color(255, 215, 0));
					cbCorCasa1.setBackground(new Color(255, 255, 0));
					cbNacionalidade1.setBackground(new Color(255, 255, 0));
					cbBebida1.setBackground(new Color(255, 255, 0));
					cbCigarro1.setBackground(new Color(255, 255, 0));
					cbAnimal1.setBackground(new Color(255, 255, 0));
					
				}else if(corCasa1.equals("Azul")){
					cbCorCasa1.setForeground(new Color(77, 77, 255));
					cbCorCasa1.setBackground(new Color(30, 144, 255));
					cbNacionalidade1.setBackground(new Color(30, 144, 255));
					cbBebida1.setBackground(new Color(30, 144, 255));
					cbCigarro1.setBackground(new Color(30, 144, 255));
					cbAnimal1.setBackground(new Color(30, 144, 255));
					
				}else if(corCasa1.equals("Laranja")){
					cbCorCasa1.setForeground(new Color(255, 165, 0));
					cbCorCasa1.setBackground(new Color(255, 140, 0));
					cbNacionalidade1.setBackground(new Color(255, 140, 0));
					cbBebida1.setBackground(new Color(255, 140, 0));
					cbCigarro1.setBackground(new Color(255, 140, 0));
					cbAnimal1.setBackground(new Color(255, 140, 0));
					
				}else if(corCasa1.equals("Verde")){
					cbCorCasa1.setForeground(new Color(112, 219, 47));
					cbCorCasa1.setBackground(new Color(0, 128, 0));
					cbNacionalidade1.setBackground(new Color(0, 128, 0));
					cbBebida1.setBackground(new Color(0, 128, 0));
					cbCigarro1.setBackground(new Color(0, 128, 0));
					cbAnimal1.setBackground(new Color(0, 128, 0));
					
				}else if(corCasa1.equals("Vermelha")){
					cbCorCasa1.setForeground(new Color(255, 0, 0));
					cbCorCasa1.setBackground(new Color(255, 99, 71));
					cbNacionalidade1.setBackground(new Color(255, 99, 71));
					cbBebida1.setBackground(new Color(255, 99, 71));
					cbCigarro1.setBackground(new Color(255, 99, 71));
					cbAnimal1.setBackground(new Color(255, 99, 71));
					
				}else{
					cbCorCasa1.setForeground(new Color(0, 0, 0));
					cbCorCasa1.setBackground(new Color(238, 238, 238));
					cbNacionalidade1.setBackground(new Color(238, 238, 238));
					cbBebida1.setBackground(new Color(238, 238, 238));
					cbCigarro1.setBackground(new Color(238, 238, 238));
					cbAnimal1.setBackground(new Color(238, 238, 238));
				}
				//======================================
			}
		});
		cbCorCasa1.setBounds(133, 36, 147, 50);
		frame.getContentPane().add(cbCorCasa1);
		
		cbBebida1 = new JComboBox();
		cbBebida1.setBounds(133, 109, 147, 50);
		frame.getContentPane().add(cbBebida1);
		
		cbCigarro1 = new JComboBox();
		cbCigarro1.setBounds(133, 146, 147, 50);
		frame.getContentPane().add(cbCigarro1);
		
		cbAnimal1 = new JComboBox();
		cbAnimal1.setBounds(133, 182, 147, 50);
		frame.getContentPane().add(cbAnimal1);
		
		cbCorCasa2 = new JComboBox();
		cbCorCasa2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				//======================================
				// COR DA CASA 2
				
				corCasa2 = cbCorCasa2.getSelectedItem().toString();
				if(corCasa2.equals("Amarela")){
					cbCorCasa2.setForeground(new Color(255, 215, 0));
					cbCorCasa2.setBackground(new Color(255, 255, 0));
					cbNacionalidade2.setBackground(new Color(255, 255, 0));
					cbBebida2.setBackground(new Color(255, 255, 0));
					cbCigarro2.setBackground(new Color(255, 255, 0));
					cbAnimal2.setBackground(new Color(255, 255, 0));
					
				}else if(corCasa2.equals("Azul")){
					cbCorCasa2.setForeground(new Color(77, 77, 255));
					cbCorCasa2.setBackground(new Color(30, 144, 255));
					cbNacionalidade2.setBackground(new Color(30, 144, 255));
					cbBebida2.setBackground(new Color(30, 144, 255));
					cbCigarro2.setBackground(new Color(30, 144, 255));
					cbAnimal2.setBackground(new Color(30, 144, 255));
					
				}else if(corCasa2.equals("Laranja")){
					cbCorCasa2.setForeground(new Color(255, 165, 0));
					cbCorCasa2.setBackground(new Color(255, 140, 0));
					cbNacionalidade2.setBackground(new Color(255, 140, 0));
					cbBebida2.setBackground(new Color(255, 140, 0));
					cbCigarro2.setBackground(new Color(255, 140, 0));
					cbAnimal2.setBackground(new Color(255, 140, 0));
					
				}else if(corCasa2.equals("Verde")){
					cbCorCasa2.setForeground(new Color(112, 219, 47));
					cbCorCasa2.setBackground(new Color(0, 128, 0));
					cbNacionalidade2.setBackground(new Color(0, 128, 0));
					cbBebida2.setBackground(new Color(0, 128, 0));
					cbCigarro2.setBackground(new Color(0, 128, 0));
					cbAnimal2.setBackground(new Color(0, 128, 0));
					
				}else if(corCasa2.equals("Vermelha")){
					cbCorCasa2.setForeground(new Color(255, 0, 0));
					cbCorCasa2.setBackground(new Color(255, 99, 71));
					cbNacionalidade2.setBackground(new Color(255, 99, 71));
					cbBebida2.setBackground(new Color(255, 99, 71));
					cbCigarro2.setBackground(new Color(255, 99, 71));
					cbAnimal2.setBackground(new Color(255, 99, 71));
					
				}else{
					cbCorCasa2.setForeground(new Color(0, 0, 0));
					cbCorCasa2.setBackground(new Color(238, 238, 238));
					cbNacionalidade2.setBackground(new Color(238, 238, 238));
					cbBebida2.setBackground(new Color(238, 238, 238));
					cbCigarro2.setBackground(new Color(238, 238, 238));
					cbAnimal2.setBackground(new Color(238, 238, 238));
				}
				//======================================
			}
		});
		cbCorCasa2.setBounds(286, 36, 147, 50);
		frame.getContentPane().add(cbCorCasa2);
		
		cbNacionalidade2 = new JComboBox();
		cbNacionalidade2.setBounds(286, 73, 147, 50);
		frame.getContentPane().add(cbNacionalidade2);
		
		cbBebida2 = new JComboBox();
		cbBebida2.setBounds(286, 109, 147, 50);
		frame.getContentPane().add(cbBebida2);
		
		cbCigarro2 = new JComboBox();
		cbCigarro2.setBounds(286, 146, 147, 50);
		frame.getContentPane().add(cbCigarro2);
		
		cbAnimal2 = new JComboBox();
		cbAnimal2.setBounds(286, 182, 147, 50);
		frame.getContentPane().add(cbAnimal2);
		
		cbCorCasa3 = new JComboBox();
		cbCorCasa3.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				//======================================
				// COR DA CASA 3
				
				corCasa3 = cbCorCasa3.getSelectedItem().toString();
				if(corCasa3.equals("Amarela")){
					cbCorCasa3.setForeground(new Color(255, 215, 0));
					cbCorCasa3.setBackground(new Color(255, 255, 0));
					cbNacionalidade3.setBackground(new Color(255, 255, 0));
					cbBebida3.setBackground(new Color(255, 255, 0));
					cbCigarro3.setBackground(new Color(255, 255, 0));
					cbAnimal3.setBackground(new Color(255, 255, 0));
					
				}else if(corCasa3.equals("Azul")){
					cbCorCasa3.setForeground(new Color(77, 77, 255));
					cbCorCasa3.setBackground(new Color(30, 144, 255));
					cbNacionalidade3.setBackground(new Color(30, 144, 255));
					cbBebida3.setBackground(new Color(30, 144, 255));
					cbCigarro3.setBackground(new Color(30, 144, 255));
					cbAnimal3.setBackground(new Color(30, 144, 255));
					
				}else if(corCasa3.equals("Laranja")){
					cbCorCasa3.setForeground(new Color(255, 165, 0));
					cbCorCasa3.setBackground(new Color(255, 140, 0));
					cbNacionalidade3.setBackground(new Color(255, 140, 0));
					cbBebida3.setBackground(new Color(255, 140, 0));
					cbCigarro3.setBackground(new Color(255, 140, 0));
					cbAnimal3.setBackground(new Color(255, 140, 0));
					
				}else if(corCasa3.equals("Verde")){
					cbCorCasa3.setForeground(new Color(112, 219, 47));
					cbCorCasa3.setBackground(new Color(0, 128, 0));
					cbNacionalidade3.setBackground(new Color(0, 128, 0));
					cbBebida3.setBackground(new Color(0, 128, 0));
					cbCigarro3.setBackground(new Color(0, 128, 0));
					cbAnimal3.setBackground(new Color(0, 128, 0));
					
				}else if(corCasa3.equals("Vermelha")){
					cbCorCasa3.setForeground(new Color(255, 0, 0));
					cbCorCasa3.setBackground(new Color(255, 99, 71));
					cbNacionalidade3.setBackground(new Color(255, 99, 71));
					cbBebida3.setBackground(new Color(255, 99, 71));
					cbCigarro3.setBackground(new Color(255, 99, 71));
					cbAnimal3.setBackground(new Color(255, 99, 71));
					
				}else{
					cbCorCasa3.setForeground(new Color(0, 0, 0));
					cbCorCasa3.setBackground(new Color(238, 238, 238));
					cbNacionalidade3.setBackground(new Color(238, 238, 238));
					cbBebida3.setBackground(new Color(238, 238, 238));
					cbCigarro3.setBackground(new Color(238, 238, 238));
					cbAnimal3.setBackground(new Color(238, 238, 238));
				}
				//======================================
			}
		});
		cbCorCasa3.setBounds(439, 36, 147, 50);
		frame.getContentPane().add(cbCorCasa3);
		
		cbNacionalidade3 = new JComboBox();
		cbNacionalidade3.setBounds(439, 73, 147, 50);
		frame.getContentPane().add(cbNacionalidade3);
		
		cbBebida3 = new JComboBox();
		cbBebida3.setBounds(439, 109, 147, 50);
		frame.getContentPane().add(cbBebida3);
		
		cbCigarro3 = new JComboBox();
		cbCigarro3.setBounds(439, 146, 147, 50);
		frame.getContentPane().add(cbCigarro3);
		
		cbAnimal3 = new JComboBox();
		cbAnimal3.setBounds(439, 182, 147, 50);
		frame.getContentPane().add(cbAnimal3);
		
		cbCorCasa4 = new JComboBox();
		cbCorCasa4.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				//======================================
				// COR DA CASA 4
				
				corCasa4 = cbCorCasa4.getSelectedItem().toString();
				if(corCasa4.equals("Amarela")){
					cbCorCasa4.setForeground(new Color(255, 215, 0));
					cbCorCasa4.setBackground(new Color(255, 255, 0));
					cbNacionalidade4.setBackground(new Color(255, 255, 0));
					cbBebida4.setBackground(new Color(255, 255, 0));
					cbCigarro4.setBackground(new Color(255, 255, 0));
					cbAnimal4.setBackground(new Color(255, 255, 0));
					
				}else if(corCasa4.equals("Azul")){
					cbCorCasa4.setForeground(new Color(77, 77, 255));
					cbCorCasa4.setBackground(new Color(30, 144, 255));
					cbNacionalidade4.setBackground(new Color(30, 144, 255));
					cbBebida4.setBackground(new Color(30, 144, 255));
					cbCigarro4.setBackground(new Color(30, 144, 255));
					cbAnimal4.setBackground(new Color(30, 144, 255));
					
				}else if(corCasa4.equals("Laranja")){
					cbCorCasa4.setForeground(new Color(255, 165, 0));
					cbCorCasa4.setBackground(new Color(255, 140, 0));
					cbNacionalidade4.setBackground(new Color(255, 140, 0));
					cbBebida4.setBackground(new Color(255, 140, 0));
					cbCigarro4.setBackground(new Color(255, 140, 0));
					cbAnimal4.setBackground(new Color(255, 140, 0));
					
				}else if(corCasa4.equals("Verde")){
					cbCorCasa4.setForeground(new Color(112, 219, 47));
					cbCorCasa4.setBackground(new Color(0, 128, 0));
					cbNacionalidade4.setBackground(new Color(0, 128, 0));
					cbBebida4.setBackground(new Color(0, 128, 0));
					cbCigarro4.setBackground(new Color(0, 128, 0));
					cbAnimal4.setBackground(new Color(0, 128, 0));
					
				}else if(corCasa4.equals("Vermelha")){
					cbCorCasa4.setForeground(new Color(255, 0, 0));
					cbCorCasa4.setBackground(new Color(255, 99, 71));
					cbNacionalidade4.setBackground(new Color(255, 99, 71));
					cbBebida4.setBackground(new Color(255, 99, 71));
					cbCigarro4.setBackground(new Color(255, 99, 71));
					cbAnimal4.setBackground(new Color(255, 99, 71));
					
				}else{
					cbCorCasa4.setForeground(new Color(0, 0, 0));
					cbCorCasa4.setBackground(new Color(238, 238, 238));
					cbNacionalidade4.setBackground(new Color(238, 238, 238));
					cbBebida4.setBackground(new Color(238, 238, 238));
					cbCigarro4.setBackground(new Color(238, 238, 238));
					cbAnimal4.setBackground(new Color(238, 238, 238));
				}
				//======================================
			}
		});
		cbCorCasa4.setBounds(592, 36, 147, 50);
		frame.getContentPane().add(cbCorCasa4);
		
		cbNacionalidade4 = new JComboBox();
		cbNacionalidade4.setBounds(592, 73, 147, 50);
		frame.getContentPane().add(cbNacionalidade4);
		
		cbBebida4 = new JComboBox();
		cbBebida4.setBounds(592, 109, 147, 50);
		frame.getContentPane().add(cbBebida4);
		
		cbCigarro4 = new JComboBox();
		cbCigarro4.setBounds(592, 146, 147, 50);
		frame.getContentPane().add(cbCigarro4);
		
		cbAnimal4 = new JComboBox();
		cbAnimal4.setBounds(592, 182, 147, 50);
		frame.getContentPane().add(cbAnimal4);
		
		cbCorCasa5 = new JComboBox();
		cbCorCasa5.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				//======================================
				// COR DA CASA 5
				
				corCasa5 = cbCorCasa5.getSelectedItem().toString();
				if(corCasa5.equals("Amarela")){
					cbCorCasa5.setForeground(new Color(255, 215, 0));
					cbCorCasa5.setBackground(new Color(255, 255, 0));
					cbNacionalidade5.setBackground(new Color(255, 255, 0));
					cbBebida5.setBackground(new Color(255, 255, 0));
					cbCigarro5.setBackground(new Color(255, 255, 0));
					cbAnimal5.setBackground(new Color(255, 255, 0));
					
				}else if(corCasa5.equals("Azul")){
					cbCorCasa5.setForeground(new Color(77, 77, 255));
					cbCorCasa5.setBackground(new Color(30, 144, 255));
					cbNacionalidade5.setBackground(new Color(30, 144, 255));
					cbBebida5.setBackground(new Color(30, 144, 255));
					cbCigarro5.setBackground(new Color(30, 144, 255));
					cbAnimal5.setBackground(new Color(30, 144, 255));
					
				}else if(corCasa5.equals("Laranja")){
					cbCorCasa5.setForeground(new Color(255, 165, 0));
					cbCorCasa5.setBackground(new Color(255, 140, 0));
					cbNacionalidade5.setBackground(new Color(255, 140, 0));
					cbBebida5.setBackground(new Color(255, 140, 0));
					cbCigarro5.setBackground(new Color(255, 140, 0));
					cbAnimal5.setBackground(new Color(255, 140, 0));
					
				}else if(corCasa5.equals("Verde")){
					cbCorCasa5.setForeground(new Color(112, 219, 47));
					cbCorCasa5.setBackground(new Color(0, 128, 0));
					cbNacionalidade5.setBackground(new Color(0, 128, 0));
					cbBebida5.setBackground(new Color(0, 128, 0));
					cbCigarro5.setBackground(new Color(0, 128, 0));
					cbAnimal5.setBackground(new Color(0, 128, 0));
					
				}else if(corCasa5.equals("Vermelha")){
					cbCorCasa5.setForeground(new Color(255, 0, 0));
					cbCorCasa5.setBackground(new Color(255, 99, 71));
					cbNacionalidade5.setBackground(new Color(255, 99, 71));
					cbBebida5.setBackground(new Color(255, 99, 71));
					cbCigarro5.setBackground(new Color(255, 99, 71));
					cbAnimal5.setBackground(new Color(255, 99, 71));
					
				}else{
					cbCorCasa5.setForeground(new Color(0, 0, 0));
					cbCorCasa5.setBackground(new Color(238, 238, 238));
					cbNacionalidade5.setBackground(new Color(238, 238, 238));
					cbBebida5.setBackground(new Color(238, 238, 238));
					cbCigarro5.setBackground(new Color(238, 238, 238));
					cbAnimal5.setBackground(new Color(238, 238, 238));
				}
				//======================================
			}
		});
		cbCorCasa5.setBounds(745, 36, 147, 50);
		frame.getContentPane().add(cbCorCasa5);
		
		cbNacionalidade5 = new JComboBox();
		cbNacionalidade5.setBounds(745, 73, 147, 50);
		frame.getContentPane().add(cbNacionalidade5);
		
		cbBebida5 = new JComboBox();
		cbBebida5.setBounds(745, 109, 147, 50);
		frame.getContentPane().add(cbBebida5);
		
		cbCigarro5 = new JComboBox();
		cbCigarro5.setBounds(745, 146, 147, 50);
		frame.getContentPane().add(cbCigarro5);
		
		cbAnimal5 = new JComboBox();
		cbAnimal5.setBounds(745, 182, 147, 50);
		frame.getContentPane().add(cbAnimal5);
		
		JLabel lblCasa1 = new JLabel("");
		lblCasa1.setIcon(new ImageIcon("/Users/juniorsilva/Documents/Desenvolvimento/Eclipse Luna/DesafioEinstein/src/imagens/casa1.png"));
		lblCasa1.setHorizontalAlignment(SwingConstants.CENTER);
		lblCasa1.setBounds(142, 6, 126, 36);
		frame.getContentPane().add(lblCasa1);
		
		JLabel lblCasa2 = new JLabel("");
		lblCasa2.setIcon(new ImageIcon("/Users/juniorsilva/Documents/Desenvolvimento/Eclipse Luna/DesafioEinstein/src/imagens/casa2.png"));
		lblCasa2.setHorizontalAlignment(SwingConstants.CENTER);
		lblCasa2.setBounds(292, 6, 135, 36);
		frame.getContentPane().add(lblCasa2);
		
		JLabel lblCasa3 = new JLabel("");
		lblCasa3.setIcon(new ImageIcon("/Users/juniorsilva/Documents/Desenvolvimento/Eclipse Luna/DesafioEinstein/src/imagens/casa3.png"));
		lblCasa3.setHorizontalAlignment(SwingConstants.CENTER);
		lblCasa3.setBounds(445, 6, 135, 36);
		frame.getContentPane().add(lblCasa3);
		
		JLabel lblCasa4 = new JLabel("");
		lblCasa4.setIcon(new ImageIcon("/Users/juniorsilva/Documents/Desenvolvimento/Eclipse Luna/DesafioEinstein/src/imagens/casa4.png"));
		lblCasa4.setHorizontalAlignment(SwingConstants.CENTER);
		lblCasa4.setBounds(598, 6, 135, 36);
		frame.getContentPane().add(lblCasa4);
		
		JLabel lblCasa5 = new JLabel("");
		lblCasa5.setIcon(new ImageIcon("/Users/juniorsilva/Documents/Desenvolvimento/Eclipse Luna/DesafioEinstein/src/imagens/casa5.png"));
		lblCasa5.setHorizontalAlignment(SwingConstants.CENTER);
		lblCasa5.setBounds(751, 6, 126, 36);
		frame.getContentPane().add(lblCasa5);
		
		lblDicas = new JLabel("DICAS");
		lblDicas.setFont(new Font("Lucida Blackletter", Font.BOLD | Font.ITALIC, 19));
		lblDicas.setHorizontalAlignment(SwingConstants.CENTER);
		lblDicas.setBounds(6, 229, 886, 24);
		frame.getContentPane().add(lblDicas);
		
		ckbxDica1 = new JCheckBox("O Norueguês vive na primeira casa.");
		ckbxDica1.setFont(new Font("Lucida Grande", Font.PLAIN, 13));
		ckbxDica1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				//======================================
				//DICA 1
				if(ckbxDica1.isSelected()){
					ckbxDica1.setText("<html><strike><font color=gray>O Norueguês vive na primeira casa.</font></strike></html>");
					
				}else{
					ckbxDica1.setText("O Norueguês vive na primeira casa.");
				}
				//======================================
			}
		});
		ckbxDica1.setBounds(6, 265, 427, 23);
		frame.getContentPane().add(ckbxDica1);
		
		ckbxDica2 = new JCheckBox("O Inglês vive na casa Vermelha.");
		ckbxDica2.setFont(new Font("Lucida Grande", Font.PLAIN, 13));
		ckbxDica2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				//======================================
				//DICA 2
				if(ckbxDica2.isSelected()){
					ckbxDica2.setText("<html><strike><font color=gray>O Inglês vive na casa Vermelha.</font></strike></html>");
					
				}else{
					ckbxDica2.setText("O Inglês vive na casa Vermelha.");
				}
				//======================================
			}
		});
		ckbxDica2.setBounds(6, 290, 427, 23);
		frame.getContentPane().add(ckbxDica2);
		
		ckbxDica3 = new JCheckBox("O Suéco têm Cães como animais de estimação.");
		ckbxDica3.setFont(new Font("Lucida Grande", Font.PLAIN, 13));
		ckbxDica3.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				//======================================
				//DICA 3
				if(ckbxDica3.isSelected()){
					ckbxDica3.setText("<html><strike><font color=gray>O Suéco têm Cães como animais de estimação..</font></strike></html>");
					
				}else{
					ckbxDica3.setText("O Suéco têm Cães como animais de estimação.");
				}
				//======================================
			}
		});
		ckbxDica3.setBounds(6, 314, 427, 23);
		frame.getContentPane().add(ckbxDica3);
		
		ckbxDica4 = new JCheckBox("O Dinamarquês bebe Chá.");
		ckbxDica4.setFont(new Font("Lucida Grande", Font.PLAIN, 13));
		ckbxDica4.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				//======================================
				//DICA 4
				if(ckbxDica4.isSelected()){
					ckbxDica4.setText("<html><strike><font color=gray>O Dinamarquês bebe Chá.</font></strike></html>");
					
				}else{
					ckbxDica4.setText("O Dinamarquês bebe Chá.");
				}
				//======================================
			}
		});
		ckbxDica4.setBounds(6, 339, 427, 23);
		frame.getContentPane().add(ckbxDica4);
		
		ckbxDica5 = new JCheckBox("A casa Verde fica do lado esquerdo da Laranja.");
		ckbxDica5.setFont(new Font("Lucida Grande", Font.PLAIN, 13));
		ckbxDica5.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				//======================================
				//DICA 5
				if(ckbxDica5.isSelected()){
					ckbxDica5.setText("<html><strike><font color=gray>A casa Verde fica do lado esquerdo da Laranja.</font></strike></html>");
					
				}else{
					ckbxDica5.setText("A casa Verde fica do lado esquerdo da Laranja.");
				}
				//======================================
			}
		});
		ckbxDica5.setBounds(6, 367, 427, 23);
		frame.getContentPane().add(ckbxDica5);
		
		ckbxDica6 = new JCheckBox("O homem que vive na casa Verde bebe Café.");
		ckbxDica6.setFont(new Font("Lucida Grande", Font.PLAIN, 13));
		ckbxDica6.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				//======================================
				//DICA 6
				if(ckbxDica6.isSelected()){
					ckbxDica6.setText("<html><strike><font color=gray>O homem que vive na casa Verde bebe Café</font></strike></html>");
					
				}else{
					ckbxDica6.setText("O homem que vive na casa Verde bebe Café");
				}
				//======================================
			}
		});
		ckbxDica6.setBounds(6, 392, 427, 23);
		frame.getContentPane().add(ckbxDica6);
		
		ckbxDica7 = new JCheckBox("O homem que fuma Pall Mall cria Pássaros.");
		ckbxDica7.setFont(new Font("Lucida Grande", Font.PLAIN, 13));
		ckbxDica7.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				//======================================
				//DICA 7
				if(ckbxDica7.isSelected()){
					ckbxDica7.setText("<html><strike><font color=gray>O homem que fuma Pall Mall cria Pássaros.</font></strike></html>");
					
				}else{
					ckbxDica7.setText("O homem que fuma Pall Mall cria Pássaros.");
				}
				//======================================
			}
		});
		ckbxDica7.setBounds(6, 416, 427, 23);
		frame.getContentPane().add(ckbxDica7);
		
		ckbxDica8 = new JCheckBox("O homem que vive na casa Amarela fuma Dunhill.");
		ckbxDica8.setFont(new Font("Lucida Grande", Font.PLAIN, 13));
		ckbxDica8.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				//======================================
				//DICA 8
				if(ckbxDica8.isSelected()){
					ckbxDica8.setText("<html><strike><font color=gray>O homem que vive na casa Amarela fuma Dunhill.</font></strike></html>");
					
				}else{
					ckbxDica8.setText("O homem que vive na casa Amarela fuma Dunhill.");
				}
				//======================================
			}
		});
		ckbxDica8.setBounds(6, 441, 427, 23);
		frame.getContentPane().add(ckbxDica8);
		
		ckbxDica9 = new JCheckBox("O homem que vive na casa do meio bebe Leite.");
		ckbxDica9.setFont(new Font("Lucida Grande", Font.PLAIN, 13));
		ckbxDica9.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				//======================================
				//DICA 9
				if(ckbxDica9.isSelected()){
					ckbxDica9.setText("<html><strike><font color=gray>O homem que vive na casa do meio bebe Leite.</font></strike></html>");
					
				}else{
					ckbxDica9.setText("O homem que vive na casa do meio bebe Leite.");
				}
				//======================================
			}
		});
		ckbxDica9.setBounds(466, 265, 427, 23);
		frame.getContentPane().add(ckbxDica9);
		
		ckbxDica10 = new JCheckBox("O homem que fuma Blends vive ao lado do que tem Gatos.");
		ckbxDica10.setFont(new Font("Lucida Grande", Font.PLAIN, 13));
		ckbxDica10.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				//======================================
				//DICA 10
				if(ckbxDica10.isSelected()){
					ckbxDica10.setText("<html><strike><font color=gray>O homem que fuma Blends vive ao lado do que tem Gatos.</font></strike></html>");
					
				}else{
					ckbxDica10.setText("O homem que fuma Blends vive ao lado do que tem Gatos.");
				}
				//======================================
			}
		});
		ckbxDica10.setBounds(466, 290, 427, 23);
		frame.getContentPane().add(ckbxDica10);
		
		ckbxDica11 = new JCheckBox("O homem que cria Cavalos é vizinho do que fuma Dunhill.");
		ckbxDica11.setFont(new Font("Lucida Grande", Font.PLAIN, 13));
		ckbxDica11.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				//======================================
				//DICA 11
				if(ckbxDica11.isSelected()){
					ckbxDica11.setText("<html><strike><font color=gray>O homem que cria Cavalos é vizinho do que fuma Dunhill.</font></strike></html>");
					
				}else{
					ckbxDica11.setText("O homem que cria Cavalos é vizinho do que fuma Dunhill.");
				}
				//======================================
			}
		});
		ckbxDica11.setBounds(466, 314, 427, 23);
		frame.getContentPane().add(ckbxDica11);
		
		ckbxDica12 = new JCheckBox("O homem que fuma Bluemaster bebe Cerveja.");
		ckbxDica12.setFont(new Font("Lucida Grande", Font.PLAIN, 13));
		ckbxDica12.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				//======================================
				//DICA 12
				if(ckbxDica12.isSelected()){
					ckbxDica12.setText("<html><strike><font color=gray>O homem que fuma Bluemaster bebe Cerveja.</font></strike></html>");
					
				}else{
					ckbxDica12.setText("O homem que fuma Bluemaster bebe Cerveja.");
				}
				//======================================
			}
		});
		ckbxDica12.setBounds(466, 339, 427, 23);
		frame.getContentPane().add(ckbxDica12);
		
		ckbxDica13 = new JCheckBox("O Alemão fuma Prince.");
		ckbxDica13.setFont(new Font("Lucida Grande", Font.PLAIN, 13));
		ckbxDica13.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				//======================================
				//DICA 13
				if(ckbxDica13.isSelected()){
					ckbxDica13.setText("<html><strike><font color=gray>O Alemão fuma Prince.</font></strike></html>");
					
				}else{
					ckbxDica13.setText("O Alemão fuma Prince.");
				}
				//======================================
			}
		});
		ckbxDica13.setBounds(466, 367, 427, 23);
		frame.getContentPane().add(ckbxDica13);
		
		ckbxDica14 = new JCheckBox("O Norueguês vive ao lado da casa Azul.");
		ckbxDica14.setFont(new Font("Lucida Grande", Font.PLAIN, 13));
		ckbxDica14.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				//======================================
				//DICA 14
				if(ckbxDica14.isSelected()){
					ckbxDica14.setText("<html><strike><font color=gray>O Norueguês vive ao lado da casa Azul.</font></strike></html>");
					
				}else{
					ckbxDica14.setText("O Norueguês vive ao lado da casa Azul.");
				}
				//======================================
			}
		});
		ckbxDica14.setBounds(466, 392, 427, 23);
		frame.getContentPane().add(ckbxDica14);
		
		ckbxDica15 = new JCheckBox("O homem que fuma Blends é vizinho do que bebe Água.");
		ckbxDica15.setFont(new Font("Lucida Grande", Font.PLAIN, 13));
		ckbxDica15.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				//======================================
				//DICA 15
				if(ckbxDica15.isSelected()){
					ckbxDica15.setText("<html><strike><font color=gray>O homem que fuma Blends é vizinho do que bebe Água.</font></strike></html>");
					
				}else{
					ckbxDica15.setText("O homem que fuma Blends é vizinho do que bebe Água.");
				}
				//======================================
			}
		});
		ckbxDica15.setBounds(466, 416, 427, 23);
		frame.getContentPane().add(ckbxDica15);
		
		JButton btnConfirmar = new JButton("Confirmar");
		btnConfirmar.setForeground(Color.BLUE);
		btnConfirmar.setFont(new Font("Apple Color Emoji", Font.PLAIN, 12));
		btnConfirmar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				//======================================
				//BOTÃO CONFIRMAR
				
				confirmar();
				//======================================
			}
		});
		btnConfirmar.setBounds(592, 442, 88, 24);
		frame.getContentPane().add(btnConfirmar);
		
		JButton btnZerar = new JButton("ZERAR");
		btnZerar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				//======================================
				//BOTÃO ZERAR
				
				zerar();
				//======================================
			}
		});
		btnZerar.setFont(new Font("Apple Color Emoji", Font.PLAIN, 12));
		btnZerar.setForeground(Color.RED);
		btnZerar.setBounds(692, 442, 98, 24);
		frame.getContentPane().add(btnZerar);
		
		lblParabéns = new JLabel("Parabéns, você conseguiu!!!");
		lblParabéns.setHorizontalAlignment(SwingConstants.CENTER);
		lblParabéns.setIcon(null);
		lblParabéns.setForeground(new Color(0, 0, 128));
		lblParabéns.setFont(new Font("Yuanti SC", Font.BOLD | Font.ITALIC, 40));
		lblParabéns.setBounds(6, 193, 886, 95);
		frame.getContentPane().add(lblParabéns);
	}
}
